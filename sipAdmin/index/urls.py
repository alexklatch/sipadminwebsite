from django.conf.urls import url
from . import views

app_name = 'index'

urlpatterns = [
    url(r'^login/$', views.user_login, name='login'),
    url(r'^change/$', views.change, name='change'),
    #url(r'^network/$', views.user_login, name='network'),
    url(r'^sip/$', views.sips, name='sip'),
    # url(r'^info/$', views.user_create, name='info'),

]
