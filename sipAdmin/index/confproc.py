import os
import configparser

class manager:

	def __init__(self, work_dir):
		self.work_dir = work_dir
		self.sip_conf_path = work_dir + 'sip.conf'
		self.sip_ext_path = work_dir + 'extensions.conf'
		self.config = configparser.ConfigParser(comment_prefixes='(', allow_no_value=True)

	def add_user(self, name, secret):
		config = self.config
		config.read(self.sip_conf_path)
		if not(name in config):
			config[name] = {}
			new_user = config[name]	
			new_user[';'] = 'user'
			new_user['secret'] = secret
			self.config = config
			self.add_user_to_ext(name)
			return 'Created'
		else:
			return 'Name is taken'

	def add_user_to_ext(self, name):
		lines = []
		with open(self.sip_ext_path, 'r') as extfile :
			lines = extfile.readlines()
		lines.append("exten=>{0},1,Dial(SIP/{0},120)".format(name))
		with open(self.sip_ext_path, 'w') as extfile:
			for item in lines:
				extfile.write("%s\n" % item)
	def commit(self):
		with open(self.sip_conf_path, 'w') as confile:
			self.config.write(confile)
		lines = []
		with open(self.sip_conf_path, 'r') as confile:
			lines = confile.readlines()
			print(lines)

			for i in range(len(lines)):
        			l = lines[i]
        			if '; = user' in l:
                			print(lines[i - 1])
                			lines[i - 1] = '{0}{1}'.format(lines[i - 1].rstrip(), "(friends_internal)")
                			print(lines[i - 1])
                			print("found")
		with open(self.sip_conf_path, 'w') as confile:
			for item in lines:
			        confile.write("%s\n" % item)

if __name__ == "__main__":

	work_dir = '/etc/asterisk/'

	sip_conf_path = work_dir + 'sip.conf'

	m = manager(work_dir)

	print(m.add_user('8003', '8003pw'))

	print(m.commit())

	print(sip_conf_path)

#f = open(sip_conf_path, 'r')
#
#lines = f.readlines()
#
#print(lines)
#
#for i in range(len(lines)):
#	l = lines[i]
#	if '; user' in l:
#		print(lines[i - 1])
#		lines[i - 1] = '{0}{1}'.format(lines[i - 1].rstrip(), "(friends_internal)")
#		print(lines[i - 1])
#		print("found")

#with open(sip_conf_path, 'w') as f:
#    for item in lines:
#        f.write("%s\n" % item)

#dirs = os.listdir(work_dir)

#print(dirs)
