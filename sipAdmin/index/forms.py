from django import forms
from index.models import SipModel
from django.contrib.auth.models import User

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','password')


class SipUserForm(forms.ModelForm):

	class Meta():
		model = SipModel
		fields = ('sip_name','sip_pass')
