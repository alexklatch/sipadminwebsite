from django.shortcuts import render
from index.forms import UserForm, SipUserForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from index.models import SipModel
from index.confproc import manager

def index(request):
    return render(request, 'index.html')

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def change(request):
    if request.method == 'POST':
        new_username = request.POST.get('username')
        new_password = request.POST.get('password')
        cur_user = list(get_user_model().objects.filter(is_superuser=True).values_list('username', flat=True))
        user = get_user_model().objects.get(username=cur_user[0])
        user.username = new_username
        user.set_password(new_password)
        user.save()
        return HttpResponseRedirect("/index/login")

    return render(request, 'change.html', {})

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("Your account was inactive.")
        else:
            return HttpResponse("Invalid login details given")
    else:
        return render(request, 'login.html', {})

def sips(request):
    if request.method == 'POST':
        form = SipUserForm(request.POST)
        form.save()
        sip_name = request.POST.get('sip_pass')
        sip_pass = request.POST.get('sip_pass')

        work_dir = '/etc/asterisk/'

        sip_conf_path = work_dir + 'sip.conf'

        m = manager(work_dir)

        print(m.add_user(sip_name, sip_pass))

        print(m.commit())

        #TODO: Send to json 
        return HttpResponseRedirect("/")

    return render(request, 'sipclient.html',{})
# Create your views here.

