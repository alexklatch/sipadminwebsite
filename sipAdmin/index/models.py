from django.db import models

class SipModel(models.Model):
    sip_name = models.CharField(max_length=255, blank=True)
    sip_pass = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.sip_name
